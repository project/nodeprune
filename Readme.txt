
Deletes/Archives old nodes

Installation
------------

Follow general drupal module instructions.  Upload the entire ranks directory to your module directory on your server generally sites/all/modules.
enable the module in your admin panel.


Author
------
Mitch Tuck
Website www.matuckcomputing.com
matuck@matuckcomputing.com

Features for 1.0 release
------------------------
Prune for different node types
on prune unpublish for certain date range
on prune delete after certain date
Filter on vocab and term
debug mode so you can see whats gonna be delted when setting up rule
manual run
enable and disable each prune rule.